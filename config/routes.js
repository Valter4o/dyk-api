const auth = require("../Auth/authRoutes");
const user = require("../User/userRoutes");
const game = require("../Game/gameRoutes");
const deck = require("../Deck/deckRoutes");
const card = require("../Card/cardRoutes");

module.exports = (app) => {
  app.use("/auth", auth);
  app.use("/user", user);
  app.use("/game", game);
  app.use("/deck", deck);
  app.use("/card", card);
  app.use("/demo", (req, res) => res.send("Ba4ka ve"));
};
