const env = process.env.NODE_ENV || "development";
const port = process.env.PORT || 6969;
const dbUrl = 'mongodb://mongo:27017/dyk';
const secret = process.env.SECRET;

const config = {
  development: {
    port,
    dbUrl,
    secret,
  },
  production: {
    port,
    dbUrl,
    secret,
  },
};

module.exports = config[env];
