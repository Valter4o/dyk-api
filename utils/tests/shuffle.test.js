const shuffle = require("../shuffle");

describe("Shuffle function tests", () => {
  test("Given long array it returns different array with same length", () => {
    const normalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const shuffledArr = shuffle(normalArr);
    expect(normalArr.length).toEqual(shuffledArr.length);
  });

  test("Given array it doesn't modify the original", () => {
    const normalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    shuffle(normalArr);
    expect(normalArr).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });

  test("Given long array it returns different array shuffled items", () => {
    const normalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const shuffledArr = shuffle(normalArr);
    expect(normalArr).not.toEqual(shuffledArr);
  });
});
