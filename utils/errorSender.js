function errorSender(res, message, status = 400) {
  res.status(status).json({
    message,
  });
}

module.exports = errorSender;
