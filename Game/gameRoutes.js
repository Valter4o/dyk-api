const router = require("express").Router();
const {
  getAllGames,
  getGameById,
  createGame,
  patchGame,
  deleteGameById,
  getAllGameDecks,
  getAllGameCards,
} = require("./gameHandler");

router.get("/", getAllGames);
router.post("/", createGame);
router.get("/:gameId", getGameById);
router.patch("/:gameId", patchGame);
router.delete("/:gameId", deleteGameById);
router.get("/:gameId/deck", getAllGameDecks);
router.get("/:gameId/card", getAllGameCards);

module.exports = router;
