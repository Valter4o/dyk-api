const { Schema, model: Model } = require("mongoose");
const { String, ObjectId } = Schema.Types;

const gameSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  explore: {
    type: Boolean,
    required: false,
  },
  shortDescription: {
    type: String,
    required: true,
  },
  explore: {
    type: Boolean,
    required: false,
  },
  decks: [
    {
      type: ObjectId,
      ref: "Deck",
    },
  ],
});

gameSchema.methods = {};

module.exports = new Model("Game", gameSchema);
