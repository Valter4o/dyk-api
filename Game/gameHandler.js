const Game = require("./GameModel");
const errorSender = require("../utils/errorSender");

module.exports = {
  getAllGames: async (req, res) => {
    try {
      const games = await Game.find();
      res.json(games).status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  getGameById: async (req, res) => {
    try {
      const { gameId } = req.params;
      const game = await Game.findOne({ _id: gameId });
      res.json(game).status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  createGame: async (req, res) => {
    try {
      const { name, explore, shortDescription } = req.body;
      const newGame = new Game({
        name,
        explore: explore ? true : false,
        shortDescription: shortDescription || "",
        decks: [],
      });
      await newGame.save();
      res.send("Success").status(300);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  patchGame: (req, res) => {
    // Todo
    res.send("patchGame");
  },
  deleteGameById: async (req, res) => {
    try {
      const { gameId } = req.params;
      await Game.deleteOne({ _id: gameId });
      res.send("Success").status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  getAllGameDecks: (req, res) => {
    // Todo
    res.send("getAllGameDecks");
  },
  getAllGameCards: (req, res) => {
    // Todo
    res.send("getAllGameCards");
  },
};
