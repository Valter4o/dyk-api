const router = require("express").Router();
const {
  getAllCards,
  createCard,
  getCardById,
  patchCard,
  deleteCard,
} = require("./cardHandler");

router.get("/", getAllCards);
router.post("/", createCard);
router.get("/:cardId", getCardById);
router.patch("/:cardId", patchCard);
router.delete("/:cardId", deleteCard);

module.exports = router;
