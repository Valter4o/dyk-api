const { Schema, model: Model } = require("mongoose");
const { String, ObjectId } = Schema.Types;

const cardSchema = new Schema({
  text: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: false,
  },
  imageUrl: {
    type: String,
    required: false,
  },
  value: {
    type: [String, Number],
    required: false,
  },
  deck: {
    type: ObjectId,
    required: true,
    ref: "Deck",
  },
  game: {
    type: ObjectId,
    required: true,
    ref: "Game",
  },
});

cardSchema.methods = {};

module.exports = new Model("Card", cardSchema);
