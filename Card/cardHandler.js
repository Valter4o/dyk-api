const Card = require("./CardModel");
const Deck = require("../Deck/DeckModel");
const errorSender = require("../utils/errorSender");

module.exports = {
  getAllCards: async (req, res) => {
    try {
      const cards = await Card.find();
      res.send(cards).status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  createCard: async (req, res) => {
    try {
      const { deckId, text, gameId, ...rest } = req.body;
      const newCard = new Card({ text, game: gameId, deck: deckId, ...rest });
      const card = await newCard.save();
      const cardId = card._id;

      const deck = await Deck.findOne({ _id: deckId });
      deck.cards.push(cardId);
      await deck.save();

      res.send("Success").status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  getCardById: async (req, res) => {
    try {
      const { cardId } = req.params;
      const card = await Card.findOne({ _id: cardId });
      res.send(card).status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  patchCard: (req, res) => {
    // Todo
    res.send("patchCard");
  },
  deleteCard: async (req, res) => {
    try {
      const { cardId } = req.params;
      await Card.deleteOne({ _id: cardId });
      res.send('Success').status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
};
