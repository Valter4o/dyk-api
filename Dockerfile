FROM node:14

# Create app directory
WORKDIR /usr/src/app

COPY package.json ./

RUN npm i

# Bundle app source
COPY . .

EXPOSE 6969

# CMD [ "npm", "run", "dev" ]
# CMD [ "npm", "run", "start" ]