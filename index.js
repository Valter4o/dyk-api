require("dotenv").config();
require("./config/db")()
  .then(() => {
    const { port } = require("./config/config");
    const http = require("http");
    const socketIo = require("socket.io");
    const app = require("express")();
    const appString = `Server is ready,listening on port ${port}`;

    const server = http.createServer(app);
    const io = socketIo(server);

    let games = {};

    io.on("connection", (socket) => {});

    require("./config/express")(app);
    require("./config/routes")(app);
    server.listen(port, () => {
      console.log("~".repeat(42));
      console.log("* " + appString + " *");
      console.log("~".repeat(42));
    });
  })
  .catch((err) => console.log(err));
