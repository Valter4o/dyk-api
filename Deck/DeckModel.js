const { Schema, model: Model } = require("mongoose");
const { String, ObjectId, Object } = Schema.Types;

const deckSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  settings: {
    type: Object,
    required: false,
  },
  cards: [
    {
      type: ObjectId,
      ref: "Card",
    },
  ],
  game: {
    type: ObjectId,
    required: true,
    ref: "Game",
  },
  creator: {
    type: ObjectId,
    ref: "User",
    required: false,
  },
  moderators: [
    {
      type: ObjectId,
      ref: "User",
    },
  ],
});

deckSchema.methods = {};

module.exports = new Model("Deck", deckSchema);
