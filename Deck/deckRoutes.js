const router = require("express").Router();
const {
  getAllDecks,
  createDeck,
  getDeckById,
  patchDeck,
  deleteDeck,
  getAllCardsFromDeck,
} = require("./deckHandler");

router.get("/", getAllDecks);
router.post("/", createDeck);
router.get("/:deckId", getDeckById);
router.patch("/:deckId", patchDeck);
router.delete("/:deckId", deleteDeck);
router.get("/:deckId/card", getAllCardsFromDeck);

module.exports = router;
