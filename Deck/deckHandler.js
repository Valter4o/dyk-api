const Card = require("../Card/CardModel");
const Game = require("../Game/GameModel");
const Deck = require("./DeckModel");
const errorSender = require("../utils/errorSender");

module.exports = {
  getAllDecks: async (req, res) => {
    try {
      const decks = await Deck.find();
      res.json(decks).status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  createDeck: async (req, res) => {
    try {
      const { name, creatorId, gameId } = req.body;

      const newDeck = new Deck({
        name,
        game: gameId,
        creator: creatorId ? creatorId : undefined,
      });
      const deck = await newDeck.save();
      const deckId = deck._id;

      const game = await Game.findOne({ _id: gameId });
      game.decks.push(deckId);
      await game.save();

      res.send("Success").status(300);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  getDeckById: async (req, res) => {
    try {
      const { deckId } = req.params;
      const deck = await Deck.findOne({ _id: deckId });
      res.json(deck).status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  patchDeck: (req, res) => {
    // Todo
    res.send("patchDeck");
  },
  deleteDeck: async (req, res) => {
    try {
    // Todo: Remove from game
      const { deckId } = req.params;
      await Deck.deleteOne({ _id: deckId });
      res.send("Success").status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
  getAllCardsFromDeck: async (req, res) => {
    try {
      const { deckId } = req.params;
      const deck = await Deck.findOne({ _id: deckId });
      const cardIds = deck.cards;
      const cards = await Card.find({
        _id: {
          $in: cardIds,
        },
      });
      res.json(cards).status(200);
    } catch (err) {
      errorSender(res, err.message, 400);
    }
  },
};
