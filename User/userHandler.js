const User = require("./UserModel");
const errorSender = require("../utils/errorSender");

module.exports = {
  getAllUsers: async (req, res) => {
    User.find().then((data) => {
      res.json(data);
    });
  },
  getUserById: (req, res) => {
    const { id } = req.params;
    User.findOne({ _id: id })
      .then((data) => {
        res.json(data);
      })
      .catch((err) => errorSender("Can\t find user"));
  },
  patchUser: (req, res) => {
    res.send("patchUser");
  },
  deleteUser: (req, res) => {
    const { id } = req.params;

    User.deleteOne({ _id: id }).then((data) => {
      res.send(data);
    });
  },
};
