const router = require("express").Router();
const {
  getAllUsers,
  getUserById,
  patchUser,
  deleteUser,
} = require("./userHandler");

router.get("/", getAllUsers);
router.get("/:id", getUserById);
router.patch("/:id", patchUser);
router.delete("/:id", deleteUser);

module.exports = router;
